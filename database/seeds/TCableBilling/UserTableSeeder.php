<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Faisal Ahamed',
            'username'	=>'admin@xfaisal.com',
            'email' => 'admin@xfaisal.com',
            'password' => bcrypt('admin1234'),
        ]);
    }
}
