<?php

use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i < 20; $i++) {
            DB::table('payments')->insert([
                
                'client_id' => $faker-> numberBetween(1,10),
                'billing_id' => $faker-> numberBetween(1,10),
                'paid_amount' => $faker->randomElement(['700', '800','1000','1400']),
                'date' => '2019-01-01'
            ]);
        }
    }
}
