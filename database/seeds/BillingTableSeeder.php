<?php

use Illuminate\Database\Seeder;

class BillingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i < 20; $i++) {
            DB::table('billings')->insert([
                
                'client_id' => $faker-> numberBetween(1,10),
                'bill_amount' => $faker->randomElement(['700', '800','1000','1400']),
                'month' => $faker->randomElement(['January', 'February','March','April','May']),
                
            ]);
        }
    }
}
