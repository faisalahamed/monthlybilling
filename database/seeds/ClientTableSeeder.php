<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i < 20; $i++) {
            DB::table('clients')->insert([
                'name' => $faker->firstName(),
                'area_id' => $faker-> numberBetween(1,10),
                'phone_no_1' => $faker->phoneNumber(),
                'phone_no_2' => $faker->phoneNumber,
                'address' => $faker->address,
                'connection_type' =>  $faker->randomElement(['home', 'office','business','govt']),
                'package_id' =>  $faker-> numberBetween(1,10),
                'client_status' => $faker->randomElement(['Active', 'Deactive']),
                'address_proof' => $faker->randomElement(['voter_id', 'passport','electricity_bill']),
                'address_proof_no' => $faker->phoneNumber,
                'device_box_no' => $faker->randomElement(['1001', '1002','1003','1004']),
            ]);
        }

    }
}
