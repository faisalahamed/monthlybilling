<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="giganet,insoulit,faisal Ahamed ">
  <meta name="author" content="Faisal Ahamed">

  <title>GigaNet</title>

  <!-- Bootstrap core CSS -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="assets/css/agency.min.css" rel="stylesheet">


</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Giganet</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#page-top">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Services</a>
          </li>
          <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#packages">Packages</a>
              </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#bdix">Server</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#team">Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
          <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="/login">Login</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container">
      <div class="intro-text">
        <div class="intro-lead-in">Be In Touch!</div>
        <div class="intro-heading text-uppercase">Cheapest Internet Package</div>
        <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#packages">Find Pakages</a>
      </div>
    </div>
  </header>

  <!-- Services -->
  <section class="page-section" id="services">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Services</h2>
          <h3 class="section-subheading text-muted">We came up with Giga Net to provide our customers uninterrupted reliable data connection 24/7! No doubt! We are here to do business but undoubtedly our primary focus is to provide our customers the highest priority first then we strongly believe our customers will help us to grow further!</h3>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-wifi fa-stack-1x "></i>

          </span>
          <h4 class="service-heading">Flexibility</h4>
          <p class="text-muted">We have various internet packages for our different consumers. You can check and decide which package will suit your need perfectly.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="far fa-check-circle fa-stack-2x "></i>

          </span>
          <h4 class="service-heading">Quality</h4>
          <p class="text-muted">We are new ISP and very ambitious so we know very well if we can keep our customers happy with the highest quality services then we will succeed!</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-phone-volume fa-stack-1x "></i>
          </span>
          <h4 class="service-heading">Support</h4>
          <p class="text-muted">If you need help please call us or email. If you have any suggestion or complain regarding our services or anything please let us know immediately.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- packages -->
  <section class="page-section" id="packages">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Packages</h2>
                <h3 class="section-subheading text-muted">Choose a plan which meets your expectation!</h3>
            </div>
        </div>
        <div class="row">
            <!-- Basic Tier -->
            <div class="col-lg-3">
                <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Basic</h5>
                        <h6 class="card-price text-center">700 BDT<span class="period">/month</span></h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>1 Mbps</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>50Mbps Youtube</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>100Mbps BDIX & FTP</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>24/7 No FUP</li>
                        </ul>
                        <a href="#" class="btn btn-block btn-primary text-uppercase">Button</a>
                    </div>
                </div>
            </div>
            <!-- Silver Tier -->
            <div class="col-lg-3">
                <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Silver</h5>
                        <h6 class="card-price text-center">800 BDT<span class="period">/month</span></h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>2 Mbps</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>50Mbps Youtube</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>100Mbps BDIX & FTP</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>24/7 No FUP</li>
                        </ul>
                        <a href="#" class="btn btn-block btn-primary text-uppercase">Button</a>
                    </div>
                </div>
            </div>
            <!-- Gold Tier -->
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Gold</h5>
                        <h6 class="card-price text-center">1000 BDT<span class="period">/month</span></h6>
                        <hr>
                        <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>3 Mbps</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>50Mbps Youtube</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>100Mbps BDIX & FTP</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>24/7 No FUP</li>
                            </ul>
                        <a href="#" class="btn btn-block btn-primary text-uppercase">Button</a>
                    </div>
                </div>
            </div>
            <!-- Platinum Tier  -->
            <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title text-muted text-uppercase text-center">Platinum</h5>
                            <h6 class="card-price text-center">1400 BDT<span class="period">/month</span></h6>
                            <hr>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>5 Mbps</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>50Mbps Youtube</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>100Mbps BDIX & FTP</li>
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>24/7 No FUP</li>
                            </ul>
                            <a href="#" class="btn btn-block btn-primary text-uppercase">Button</a>
                        </div>
                    </div>
                </div>

            <div class="row">

                    <div class="col-lg-12 pt-5" >
                        <h4>*New Connection Fee 1000 BDT</h4>
                        <h5>* Please contact us if you’re looking for Dedicated Bandwidth or Shared Package above 5Mbps.</h5>
                    </div>

            </div>

        </div>
    </div>

  </section>


  <!-- BDIX -->
  <section class="page-section" id="bdix">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Server List</h2>
          <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 card text-white bg-secondary  " >
            <div class="card-header">Movie Server</div>
            <div class="card-body">
                <h4 class="card-title">Movie Server</h4>
                <p class="card-text">
                    <a target="_blank" class="" href="http://nagordola.com.bd">http://nagordola.com.bd</a>
                </p>
            </div>
        </div>
        <div class="col-lg-4  card text-white bg-success ml-1 " >
            <div class="card-header">Tv Server</div>
            <div class="card-body">
                <h4 class="card-title">IP TV</h4>
                <p class="card-text">
                    <a target="_blank"  class="" href="http://iptv.carnival.com.bd">http://iptv.carnival.com.bd</a>
                    <br>
                    প্রিয় গ্রাহক Live Tv সার্ভারটি লগিং এ ক্লিক করে অবশ্যই রেজিস্ট্রেশন করে নিবেন...যেভাবে রেজিস্ট্রেশন করবেন...
                    <br>
                    ১| নাম
                    <br>
                    ২| ইমেল
                    <br>
                    ৩| +880 দিয়ে ফোন নাম্বার
                    <br>
                    ৪| পাসওয়ার্ড ২ বার...<br>তারপর কন্টিনিউ।
                    কন্টিনিউ এ দেয়ার পর আপনার ফোন নাম্বারে একটি কোর্ড আসবে...কোর্ডটি বসিয়ে
                    উপভোগ করুন LIVE TV চ্যানেল।
                </p>
            </div>
        </div>
        <div class="col-lg-3  card text-white bg-info ml-1" >
            <div class="card-header">BDIX</div>
            <div class="card-body">
                <h4 class="card-title">BDIX</h4>
                <p class="card-text">
                    <a target="_blank" class="" href="http://www.crazyhd.com/">http://www.crazyhd.com</a>
                    <a target="_blank" class="" href="https://www.torrentbd.com/">https://www.torrentbd.com</a>
                </p>
            </div>
        </div>
        </div>
    </div>
  </section>

  <!-- Team -->
  <section class="bg-light page-section" id="team">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Our Amazing Team</h2>
          <h3 class="section-subheading text-muted">Dedicated to Provide Best Service</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="assets/img/team/1.jpg" alt="">
            <h4>Shohag DX</h4>
            <p class="text-muted">CEO</p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="assets/img/team/2.jpg" alt="">
            <h4>Md Ashifur Rahman</h4>
            <p class="text-muted">IT Officer</p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="assets/img/team/3.jpg" alt="">
            <h4>MD Tamim Akhand</h4>
            <p class="text-muted">Manager</p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-3">
            <div class="team-member">
                <img class="mx-auto rounded-circle" src="assets/img/team/4.jpg" alt="">
                <h4>Adv MD Sajeeb</h4>
                <p class="text-muted">Legal Advisor</p>
                <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                    <a href="#">
                    <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#">
                    <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#">
                    <i class="fab fa-linkedin-in"></i>
                    </a>
                </li>
                </ul>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
        </div>
      </div>
    </div>
  </section>


  <!-- Contact -->
  <section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Contact Us</h2>
          <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact form JavaScript -->
  <script src="assets/js/jqBootstrapValidation.js"></script>
  <script src="assets/js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="assets/js/agency.min.js"></script>

</body>

</html>
